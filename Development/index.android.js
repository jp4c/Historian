import { AppRegistry, Text } from 'react-native';

import App from './app.js';

AppRegistry.registerComponent('Historian', () => App);
