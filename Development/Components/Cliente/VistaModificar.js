import React, {Component} from 'react';
import { StyleSheet, Text, View ,Alert} from 'react-native';

import { Icon,FormLabel, FormInput,FormValidationMessage,Button} from 'react-native-elements';


class VistaModificar extends Component {
  //constructor(props){
  //  super(props);
  //}
  //static navigationOptions = ({ navigation }) => ({
    //title: `Chat with ${navigation.state.params.user}`,
  //});


  static navigationOptions = ({ navigation }) => ({
    title: 'Registrar Historia',
    headerLeft: <Icon
                  name='arrow-back'
                  size = {25}
                  style = {
                    { marginLeft: 10 }

                  }
                  onPress={ () => {
                    alert("presiona atras")
                    alert(JSON.stringify(navigation))
                    //navigation.goBack('Screen4');
                    }
                  }
                 />,
  });

  async Click()  {
    try {
      let response = await fetch("http://192.168.1.50:3000/persona/ingresar", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify ({cedula:this.state.cedula, nombres: this.state.nombres, apellidos:this.state.apellidos, edad:this.state.edad,telefono:this.state.telefono, direccion:this.state.direccion})

      });


      let responseJson = await response.json();

      alert("sucess "+responseJson.mensaje);

    } catch(error) {
      console.error(error);
    }
  }

  render() {

    return (
      <View>
      <FormInput
      placeholder = "Cedula"
      onChangeText={(cedula) => this.setState({cedula})}
      />
      <FormInput
      placeholder = "Nombres"
      onChangeText={(nombres) => this.setState({nombres})}
      />
      <FormInput
      placeholder = "Apellidos"
      onChangeText={(apellidos) => this.setState({apellidos})}

      />
      <FormInput
      placeholder = "Edad"
      onChangeText={(apellidos) => this.setState({apellidos})}

      />
      <FormInput
      placeholder = "Telefono"
      onChangeText={(apellidos) => this.setState({apellidos})}

      />
      <FormInput
      placeholder = "Direccion"
      onChangeText={(apellidos) => this.setState({apellidos})}

      />


      <Button
      onPress={this.Click.bind(this)}
      title='BUTTON' />

      </View>

    );

  }
}
export default VistaModificar;
