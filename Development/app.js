import { StackNavigator } from 'react-navigation';

import DrawerStack from './Components/drawerStack.js';
import ModificarCliente from './Components/Cliente/VistaModificar.js';

const GestionHistoriaStack = StackNavigator({
    ModificarCliente : {screen : ModificarCliente}
}, {
  headerMode: 'screen'
  
});

const Navigator = StackNavigator({//objeto navigator

        drawerStack : {screen : DrawerStack},
        historia : {screen : GestionHistoriaStack}
// se puede añadir otro conjunto de pantallas como un login
        },{
//propiedades que queremos modificar
        headerMode : 'none',
        initalRouteName : 'drawerStack' //ruta principal


})

export default Navigator;
