import React, {Component} from 'react';
import{View, Text, Alert, ListView,StyleSheet, Image} from 'react-native';
import { Icon,FormLabel, FormInput,FormValidationMessage,Button,List, ListItem } from 'react-native-elements';


class Gestion_Cliente extends Component {
  constructor(props){
    super(props);


    this.state = {
      cedula:'',
      nombres:'',
      apellidos:'',
      edad:'',
      telefono:'',
      direccion:'',
      datasource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1 != r2})

    }

  }
  async componentDidMount(){
    try{
      let response = await fetch("https://app-bootes.herokuapp.com/persona/listar", {
        method: "POST"

      });
      let responseJson = await response.json();
      this.setState({
        datasource: this.state.datasource.cloneWithRows(responseJson)


      })
      //alert("sucess "+responseJson.mensaje);
    }catch(error) {
      console.error(error);
    }


  }


  render() {

    const { navigate } = this.props.navigation;

    return(
      <View style = {styles.container}>

        <ListView
        dataSource = {this.state.datasource}
        renderRow = {(rowData) =>

          <View style = {styles.boxContainer}>

          <Image source={{ uri: 'https://myslu.slu.edu/res/images/cas-padlock-icon.png'}} style={styles.photo} />
          <Text style = {styles.text}>
          {rowData.nombres}
          </Text>

          <Icon
          style = {styles.icon}
          name='account-box'
          size = {30}
          onPress={() =>
            alert("Presiona Detalles!")


          } />
          <Icon
          style = {styles.icon}
          name='edit'
          size = {30}
          onPress={() => navigate('ModificarCliente')} />

          <Icon
          style = {styles.icon}
          name='delete'
          size = {30}
          onPress={() => alert("Presiona Eliminar!")} />
          </View>
        }

        />

      </View>
    );

  }


}
const styles = StyleSheet.create({

  container:{
    flex:1,
    flexDirection: 'column',

  },
  boxContainer:{
    flex:1,
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    borderColor: '#000033',
    borderWidth: 1,
    borderRadius: 5
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  photo: {
    height: 50,
    width: 50,
    borderRadius: 20
  },
  icon:{
    flex:1,
    alignItems: 'flex-end'


  }
});

export default Gestion_Cliente;
